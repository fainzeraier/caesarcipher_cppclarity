#include <iostream>
#include "CaesarCipher.h"
#include <string>
using namespace std;

void InterpretInput(int argument_count, char *argument_values[]){
   if (argument_count != 4)
   {
      printf("Error: expected 4 arguments, got %i", argument_count);
      return;
   }

   // strtol variables
   const char *message_pointer = argument_values[1];
   char *end_pointer = NULL;
   long encryption_key = 0;

   errno = 0; // reset errno to 0 before call

   // strtol attempts to convert message to long, and sets end_pointer and errno to use for error checking
   encryption_key = strtol(message_pointer, &end_pointer, 10);

   // Check problems. 1. if message_pointer and end_pointer are same, no characters were read
   if (message_pointer == end_pointer)
   {
      printf("Encryption key invalid  (no digits found, 0 returned)\n");
      return;
   }
   // 2. Check for overflow and underflow
   else if (errno == ERANGE && (encryption_key == LONG_MIN || encryption_key == LONG_MAX))
   {
      printf("Encryption key invalid  (underflow or overflow occurred)\n");
      return;
   }
   // 3. If some other error happened, check if received key is 0
   else if (errno != 0 && encryption_key == 0)
   {
      printf("Encryption key invalid (unspecified error occurred)\n");
      return;
   }

   bool encrypt;
   string crypt_instruction = string(argument_values[2]);

   // strcmp returns 0 only when the strings match
   if (crypt_instruction == "crypt")
   {
      encrypt = true;
   }
   else if (crypt_instruction == "decrypt")
   {
      encrypt = false;
   }
   else
   {
      printf("Error: did not receive either 'crypt' or 'decrypt' instruction");
      return;
   }

   string message = string(argument_values[3]);
   string modified_message;

   // Create cipher object, then encrypt or decrypt with it
   CaesarCipher cipher = CaesarCipher(encryption_key);
   if (encrypt)
   {
      modified_message = cipher.Encrypt(message);
   }
   else
   {
      modified_message = cipher.Decrypt(message);
   }

   printf(modified_message.c_str());
}

int main(int argc, char *argv[])
{
   InterpretInput(argc, argv);
   return 0;
}
