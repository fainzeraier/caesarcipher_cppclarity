#include "CaesarCipher.h"

CaesarCipher::CaesarCipher(int encryption_key)
{
   if(encryption_key >= 0){
      encryption_key = encryption_key % 26;
   }
   else{
      encryption_key = 26 - (-1 * encryption_key)%26;
   }

   // TODO: check whether the key is negative or >25. Bring it to 0..25 range to make things easier and give message?
   this->encryption_key = encryption_key;
}

int CaesarCipher::getEncryptionKey()
{
   return encryption_key;
}

string CaesarCipher::LeftShift(int left_shift, string message)
{
   int message_length = message.length();
   string shifted_message = message;
   
   bool contains_letter_outside_bounds = false;
   for (int i = 0; i < message_length; i++)
   {
      
      if (message[i] < 'a' || message[i] > 'z')
      {
         contains_letter_outside_bounds = true;
      }
      else
      {
         // Do left shift, then if below 'a', increase by 26 to bring it back within a-z range.
         shifted_message[i] -= left_shift;
         if (shifted_message[i] < 'a')
         {
            shifted_message[i] += 26;
         }
      }
   }
   if (contains_letter_outside_bounds)
   {
      printf("Warning: The message contained one or more letters outside a-z bounds. They were not altered.");
   }
   return shifted_message;
}

string CaesarCipher::Encrypt(string message)
{
   // Left shifting by 26-key is the same as right shifting by key.
   int left_shift = 26 - encryption_key % 26;
   return LeftShift(left_shift, message);
}

string CaesarCipher::Decrypt(string message)
{
   int left_shift = encryption_key % 26;
   return LeftShift(left_shift, message);
}
