///-----------------------------------------------------------------------------
///
///\copyright License: You may use the code in this tech note for any purpose, with the understanding that it comes with NO WARRANTY.
///
///\file    CaesarCipher.h
///\brief   Header for Caesar Cipher containing Encrypt and Decrypt functions
///
///\author  Jera Design (http://www.jera.com/techinfo/jtns/jtn002.html)
///\date    ?
///
///\details A MinUnit test case is just a function that returns 0 (null) if the tests pass.
///         If the test fails, the function should return a string describing the failing.
///         test.mu_assert is simply a macro that returns a string if the expression passed to it is false.
///         The mu_runtest macro calls another test case and returns if that test case fails. That's all there is to it!
///
///-----------------------------------------------------------------------------

#ifndef minunit_h
#define minunit_h

#define mu_assert(message, test) \
   do                            \
   {                             \
      if (!(test))               \
         return message;         \
   } while (0)

#define mu_run_test(test)     \
   do                         \
   {                          \
      string message = test();\
      tests_run++;            \
      if (message != "")      \
         return message;      \
   } while (0)

int tests_run = 0;

#endif