#include "CaesarCipher.h"
#include "minunit.h"
#include <string>
using namespace std;

static string TestEncrypt()
{
   CaesarCipher cipher = CaesarCipher(4);
   string message = "attackatonce";
   string expected_result = "exxegoexsrgi";
   string result_message = cipher.Encrypt(message);
   mu_assert("error, Encrypt 4 attackatonce did not return exxegoexsrgi", result_message == expected_result);
   return "";
}

static string TestDecrypt()
{
   CaesarCipher cipher = CaesarCipher(4);
   string message = "exxegoexsrgi";
   string expected_result = "attackatonce";
   string result_message = cipher.Decrypt(message);
   //mu_assert("error, Decrypt 4 'exxegoexsrgi' did not return 'attackatonce'", result_message == expected_result);
   return "";
}

static string TestDecryptWrap()
{
   CaesarCipher cipher = CaesarCipher(31);
   string message = "abcde";
   string expected_result = "vwxyz";
   string result_message = cipher.Decrypt(message);
   mu_assert("error, Decrypt 31 'abcde' did not return 'vwxyz'", result_message == expected_result);
   return "";
}

static string TestEncryptWrap()
{
   CaesarCipher cipher = CaesarCipher(31);
   string message = "abcde";
   string expected_result = "vwxyz";
   string result_message = cipher.Decrypt(message);
   mu_assert("error, Encrypt 31 'abcde' did not return 'vwxyz'", result_message == expected_result);
   return "";
}

static string TestEncryptNegative()
{
   CaesarCipher cipher = CaesarCipher(-22);
   string message = "attackatonce";
   string expected_result = "exxegoexsrgi";
   string result_message = cipher.Encrypt(message);
   mu_assert("error, Encrypt -22 'attackatonce' did not return 'exxegoexsrgi'", result_message == expected_result);
   return "";
}



static string AllTests()
{
   mu_run_test(TestEncrypt);
   mu_run_test(TestDecrypt);

   mu_run_test(TestEncryptWrap);
   mu_run_test(TestDecryptWrap);

   mu_run_test(TestEncryptNegative);

   /* TODO: Add input tests
   mu_run_test(TestInput);
   mu_run_test(TestInputEmpty);
   */

   return "";
}

int main(int argc, char *argv[])
{
   string result = AllTests();
   if (result != "")
   {
      printf("%s\n", result);
   }
   else
   {
      printf("\nALL TESTS PASSED");
   }
   bool b = "asd" == "bbq";

   printf("\nTests run: %d\n\n", tests_run);
   
   return (result == "") ? 0 : 1;
}
