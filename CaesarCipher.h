///-----------------------------------------------------------------------------
///
///\copyright Copyright(C) Henrik Hyvärinen I guess
///
///\file    CaesarCipher.h
///\brief   Caesar Cipher class for encrypting and decrypting messages
///
///\author  Henrik Hyvärinen
///\date    9.2.2019
///
///\details Class with functions for encrypting and decrypting messages using
///         the Caesar Cipher (https://en.wikipedia.org/wiki/Caesar_cipher). It
///         shifts all characters right when encrypting, and left when
///         decrypting. The implementation alters the message directly and does
///         not return any values.
///-----------------------------------------------------------------------------

#ifndef CaesarCipher_h
#define CaesarCipher_h

#include <iostream>
#include <string>
using namespace std;

///\brief Caesar cipher for encrypting and decrypting messages
///
///\details The caesar cipher class instantiates a cipher object with
///         a given encryption key. The encryption method is shifting
///         each character right steps equal to the key value. The
///         encryption wraps around the alphabet, so e.g. 1 step right
///         from 'z' is 'a'. Decryption shifts characters left.
class CaesarCipher
{
 private:
   /// The encryption key (right shift letter by this when encrypting).
   int encryption_key;

   string LeftShift(int left_shift, string message);

 public:
   /// Create an instance of CaesarCipher with an encryption key value.
   CaesarCipher(int encryptionKey);

   /// Get the encryption key. Note that it's set in constructor and can't be modified
   int getEncryptionKey();

   ///\brief Encrypt a message by shifting every character in the message right by encryptionKey places.
   ///   
   ///\param message The message to encrypt. Assumed to contain only lowercase characters from a to z
   string Encrypt(string message);

   ///\brief Decrypt a message by shifting every character in the message left by encryptionKey places.
   ///
   ///\param message The message to decrypt. Assumed to contain only lowercase characters from a to z
   string Decrypt(string message);
};

#endif
